#include <Key.h>
#include <Keypad.h>

#include <Joystick.h>

const byte ROWS = 3;
const byte COLS = 3;

char buttons[ROWS][COLS] = {
  { '0' , '1', '2' },
  { '3' , '4', '5' },
  { '6' , '7', '8' }
};

byte rowPins[ROWS] = { 2, 3, 4 };
byte colPins[COLS] = { 5, 6, 7 };

Keypad buttonsPad = Keypad( makeKeymap(buttons), rowPins, colPins, ROWS, COLS );

const int XYPadPins[2] = { A0, A1 }; 

void setup() {
  Joystick.begin();
}

float convertAnalogRange(float x, float in_min, float in_max, float out_min, float out_max, bool isDPad)
{
  if(isDPad && x != in_min && x != in_max)
  {
    return 0;
  }
  
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void gamepad(){
  if(buttonsPad.getKeys())
  {
    for(int i = 0; i < LIST_MAX; i++)
    {
      if(buttonsPad.key[i].stateChanged)
      {
        switch (buttonsPad.key[i].kstate) 
        {
          case PRESSED:
            Joystick.setButton(i, 1);
            break;
          case RELEASED:
            Joystick.setButton(i, 0);
            break;  
        }
      }
    }
  }

  float value = analogRead(XYPadPins[0]);
  value = convertAnalogRange(value, 0, 1023, -127, 127, true);
  Joystick.setXAxis(value);

  value = value = analogRead(XYPadPins[1]);
  value = convertAnalogRange(analogRead(A1), 0, 1023, -127, 127, true);
  Joystick.setYAxis(value);
}

void loop() {
  gamepad();
}
